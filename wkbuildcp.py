
import os
import shutil
import sys

# Usage: python wkbuildcp.py WebKitBuild/Release ~/wkbuild

SRC=os.path.abspath(sys.argv[1])
DST=os.path.expanduser(os.path.abspath(sys.argv[2]))

DEBUG=os.getenv("DEBUG")

if os.path.exists(DST):
    shutil.rmtree(DST)
os.makedirs(DST)

WK2_PROGRAMS=[
    'bin/MiniBrowser',
    'bin/WebKitWebProcess',
    'bin/WebKitNetworkProcess',
]

WK2_LIBS=[
    'lib/libwebkit2gtk-4.1.so',
    'lib/libwebkit2gtk-4.1.so.0',
    'lib/libwebkit2gtkinjectedbundle.so'
]

LIBS=[
    'lib/libjavascriptcoregtk-4.1.so',
    'lib/libjavascriptcoregtk-4.1.so.0',
]

WPE_LIBS=[
    'lib/libWPEWebKit-1.1.so',
    'lib/libWPEWebKit-1.1.so.0',
    'lib/libWPEInjectedBundle.so'
]

WPE_PROGRAMS=[
    'bin/WPENetworkProcess',
    'bin/WPEWebProcess',
    'bin/MiniBrowser'
]

def dbg(msg, *args):
    if not DEBUG:
        return
    if isinstance(msg, Exception):
        print(str(msg))
    else:
        print(msg % args)

def cp(path, dst):
    fullPath = path
    if os.path.isabs(path):
        idx = len(SRC) +1
        path = path[idx:]
    realdest = os.path.join(dst, os.path.dirname(path))
    dbg("%s -> %s", path, realdest)
    if not os.path.exists(realdest):
        os.makedirs(realdest)
    shutil.copy2(fullPath, realdest)
    return os.path.join(realdest, os.path.basename(path))

def cmd(*args):
    cmd = " ".join(args)
    dbg("Exec: %s", cmd)
    os.system(cmd)

def copy_programs(programs):
    for path in programs:
        try:
            destpath = cp(os.path.join(SRC, path), DST)
        except IOError as err:
            dbg(err)
            continue

def copy_libs(libs):
    for relPath in libs:
        path = os.path.join(SRC, relPath)
        if os.path.islink(path):
            try:
                destpath = cp(os.path.realpath(path), DST)
            except IOError as err:
                dbg(err)
                continue
            idx = len(SRC) +1
            linkpath = os.path.join(DST, path[idx:])
            targetfilename = os.path.basename(os.path.realpath(path))
            cmd("ln", "-s", "-n", targetfilename, linkpath)
        else:
            try:
                destpath = cp(path, DST)
            except IOError as err:
                dbg(err)
                continue
        cmd("strip", "-s", "-p", destpath)

if os.getenv("WPE"):
    copy_libs(WPE_LIBS)
    copy_programs(WPE_PROGRAMS)
else:
    copy_libs(LIBS)
    copy_libs(WK2_LIBS)
    copy_programs(WK2_PROGRAMS)

print("Build copied from %s to %s" % (SRC, DST))
