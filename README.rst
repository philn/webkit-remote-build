
WebKit remote build scripts
===========================

WebKitGTK+
----------

On remote build machine:

::

   $ build-webkit --gtk
   $ python wkbuildcp.py WebKitBuild/Release ~/wkbuild /path/to/testmachines/home/user/wkbuild/

The last argument should be a valid path to the wkbuild directory on the test machine.
On test machine, fetch build binaries with rsync over SSH:

::

   $ cd WebKit
   $ mkdir -p WebKitBuild/GTK/Release
   $ rsync --progress -e "ssh" -avz buildbox:wkbuild/ WebKitBuild/GTK/Release

And run MiniBrowser from the Flatpak SDK runtime:

::

   $ Tools/Scripts/run-minibrowser --gtk https://webkitgtk.org

WPEWebKit
---------

The procedure is very similar to WebKitGTK+, the main difference is that
wkbuildcp.py needs to be invoked with the WPE=1 environment variable.

On remote build machine:

::

   $ build-webkit --wpe
   $ env WPE=1 python wkbuildcp.py WebKitBuild/Release ~/wkbuild /path/to/testmachines/home/user/wkbuild/

The last argument should be a valid path to the wkbuild directory on the test machine.
On test machine, fetch build binaries with rsync over SSH:

::

   $ cd WebKit
   $ mkdir -p WebKitBuild/WPE/Release
   $ rsync --progress -e "ssh" -avz buildbox:wkbuild/ WebKitBuild/WPE/Release

And run MiniBrowser from the Flatpak SDK runtime:

::

   $ Tools/Scripts/run-minibrowser --wpe https://wpewebkit.org
